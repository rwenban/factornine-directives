'use strict';

describe('Controller: OneCtrl', function () {

  // load the controller's module
  beforeEach(module('directivesApp'));

  var OneCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OneCtrl = $controller('OneCtrl', {
      $scope: scope
    });
  }));

//  it('should attach a list of awesomeThings to the scope', function () {
//    expect(scope.awesomeThings.length).toBe(3);
//  });
});
