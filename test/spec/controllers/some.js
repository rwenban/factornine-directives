'use strict';

describe('Controller: SomeCtrl', function () {

  // load the controller's module
  beforeEach(module('directivesApp'));

  var SomeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
	  SomeCtrl = $controller('SomeCtrl', {
      $scope: scope
    });
  }));

	it('silly test', inject(function ( ) {
		expect( 1 ).toBeGreaterThan( 0 );
	}));
});
