'use strict';

angular.module('directivesApp')
	.directive('myDirective', function () {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				element.text('this is the myDirective directive');

				console.log('attrs: ', attrs );
			}
		};
	});
