'use strict';

angular.module('directivesApp')
  .directive('mouseEnter', function () {
    return {
      template: '<div></div>',
      restrict: 'EA',
      link: function postLink(scope, element ) {
        element.text('This element will be high-lighted on mouse over: ', scope, element);

	      element.bind('mouseenter', function() {
		      element.css('background', 'yellow');
	      });
	      element.bind('mouseleave', function() {
		      element.css('background', 'none');
	      });
      }
    };
  });
