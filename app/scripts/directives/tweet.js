'use strict';

var app = angular.module('directivesApp');

app.directive('tweet', [ 'api', function (api) {
		return function ($scope, $log, $element, $attributes) {

			// just log to remove jshint error
			$log.info('$element, $attributes', $element, $attributes );

			$scope.retweet = function () {
				api.retweet($scope.tweet); //each scope inherits from it's parent so we still have access to the full tweet object...
			};

			$scope.reply = function () {
				api.replyToTweet($scope.tweet);
			};
		};
	}]);
