'use strict';

var app = angular.module('directivesApp');

app.directive('myAwesomeDirective', [  function (  ) {
	return {
		templateUrl: 'views/awesomeTemplate.html',




		restrict: 'A',
		scope: {
			'data': '='
		},

		controller: 'SomeCtrl',

		link: function ( scope  ) {

			console.log( 'link: ' + scope.data );

		}



		// link
//		link: function postLink(scope, element, attrs) {
//			element.text('This is the "myAwesomeDirective" directive');
//
//			$log.info('attrs: ', attrs );
//		}
	};
}]);
