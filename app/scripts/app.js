'use strict';

angular.module('directivesApp', [
		'ngRoute',
		'ngCookies',
		'ngResource',
		'ngSanitize',
		'ngRoute'
	])
	.config(function ($routeProvider, $locationProvider) {

		$routeProvider
			.when('/', {
				templateUrl: 'views/main.html',
				controller: 'MainCtrl'
			})
			.when('/one', {
				templateUrl: 'views/one.html',
				controller: 'OneCtrl'
			})
			.when('/tweet', {
				templateUrl: 'views/tweet.html',
				controller: 'TweetCtrl'
			})
			.when('/awesome', {
				templateUrl: 'views/awesome.html',
				controller: 'AwesomeCtrl'
			}).when('/mouseEnter', {
				templateUrl: 'views/mouseenter.html',
				controller: 'MouseenterCtrl'
			})
			.otherwise({
				redirectTo: '/'
			});

		$locationProvider.html5Mode(true);
	});
