'use strict';

angular.module('directivesApp')
	.controller('NavbarCtrl', function ($scope, $location) {

		$scope.menu = [
			{
				'title': 'Home',
				'link': '/'
			},
			{
				'title': 'One',
				'link': '/one'
			},
			{
				'title': 'Tweet',
				'link': '/tweet'
			},
			{
				'title' : 'Awesome',
				'link' : '/awesome'
			},
			{
				'title' : 'Mousey Mousey',
				'link' : '/mouseEnter'

			}
		];

		$scope.isActive = function (route) {
			return route === $location.path();
		};
	});
