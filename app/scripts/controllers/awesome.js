'use strict';

angular.module('directivesApp')
	.controller('AwesomeCtrl', ['$scope', '$log', function ($scope, $log) {

		$log.info('Hi from the AwesomeCtrl');

		$scope.awesomeThings = [

		];
	}]);
