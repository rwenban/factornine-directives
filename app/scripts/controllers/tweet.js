'use strict';

angular.module('directivesApp')
	.controller('TweetCtrl', function ($scope) {
		var data = [
			{'author': 'mrvdot', 'text': 'Check out my new Angular widget!'},
			{'author': 'russell', 'text': 'I love directives!'}
		];
		$scope.tweets = data;
	});
