# Factornine - Directives

## The purpose of this application is to create a working Angular app which can test various types of directives.

I believe in scaling downwards as well as upwards, this 'app' has full Yeoman functionality and all the [AngularJS generator](https://github.com/yeoman/generator-angular#controller) commands can be used.

Also, I'm using the [bower-angular-route](https://github.com/angular/bower-angular-route).

See my Gist - [Angular - ng-route - navbar](https://gist.github.com/russellf9/58a93ea7768db6d64b9d)
